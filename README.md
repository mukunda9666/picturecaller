# PictureCaller (beta version)

Picture based speed dial for elderly.

## Screenshots
---------------------------------------

### Home

![Home](images/home-30.jpg "Home Screen") 

### List

![List](images/contact-list-30.jpg "Contact List Screen") 

### Add Contact

![Add](images/add-contact-30.jpg "Add Contact Screen") 

### Settings

![Settings](images/settings-30.jpg "Settings Screen")

### Developer

![Developer](images/info-30.jpg "Info Screen")


## Credits
----------------------------------

Many Online Tutorials and wonderful tools.

[icons8](https://icons8.com/)

[vecteezy](https://www.vecteezy.com/)

[Lunacy](https://icons8.com/lunacy)

[Flutter](https://flutter.dev/)

[Youtube](https://www.youtube.com/results?search_query=Flutter)

## Binaries
----------------------------------

Platform

- [ARMEABI-V7a](binaries/app-armeabi-v7a-release.apk)

- [ARM64-V8A](binaries/app-arm64-v8a-release.apk)

- [X86-64](binaries/app-x86_64-release.apk)


Tip: Vist this link to check your phone instruction set
    
- [device spec](https://www.devicespecifications.com/en/model/8e9f51e8)

Look For Instruction set.

## Installation 
---------------------------------

1. Download binary relative to your phone cpu-chipset
2. FileManager -> Downloads -> app-{platform}-release.apk. Open apk
3. Accept warning unknown sources.
4. Allow filemanager to run apk.
5. Deny Google Play notification.
6. App installed and ready. :)

## Feedback
----------------------------------

mail: rudra.app.developer@gmail.com

## Disclaimer
----------------------------------

Worked for me, should as well work for you too.
Still No Guarantee, Try at your risk.

